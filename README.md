# gymbookingbot

### setup
1. install BeautifulSoup: `conda install beautifulsoup4`
2. `cp config.py.tmp config.py` and modify the credentials
3. specify the constraints in `constraints.txt`. It should contain one class per line, and currently supports constraints for time, date and day of week.
4. run `python main.py`
