import time
import datetime
from requests import Session
from bs4 import BeautifulSoup as bs
import smtplib, ssl
import config


CONSTRAINT_FILE = './constraints.txt'
MIN_TIME_AHEAD = 15  # in minutes, classes that start in less than MIN_TIME_AHEAD mins will not be booked
REFRESH_INTERVAL = 600  # in seconds

## result status
BOOKED = 0
WAITLISTED = 1
QUEUE_FULL = 2
CONSTRAINT_NOT_FULFILLED = 3
BAD_FAIL = -1


class ClassBooker(object):
    """docstring for ClassBooker."""

    def __init__(self, constraints=None, constraint_file=None):
        super(ClassBooker, self).__init__()
        self.constraints = constraints
        self.constraint_file = constraint_file

    def reset(self):
        if hasattr(self, 'sess'):
            self.sess.close()
        self.sess = Session()

    def load_constraints(self):
        self.constraints = []
        with open(self.constraint_file,'r') as f:
            for line in f:
                line = line.strip()
                # comments and empty lines
                if not line or line[0] == '#':
                    continue

                cstr = {}
                for kv in line.split(','):
                    k, v = kv.strip().split(':', 1)
                    if 'class_name' in k:
                        cstr[k.strip()] = v.strip()
                        continue

                    if '&' in v:
                        vs = [vi.strip() for vi in v.split('&')]
                        cstr[k.strip()] = ('&', vs)
                    elif '|' in v:
                        vs = [vi.strip() for vi in v.split('|')]
                        cstr[k.strip()] = ('|', vs)
                    else:
                        cstr[k.strip()] = (None, [v.strip()])
                self.constraints.append(cstr)

    def login(self):
        login_link = "https://oxfordmembers.buzzgym.co.uk/site/login"
        site = self.sess.get(login_link)
        site_content = bs(site.content, "html.parser")

        token = site_content.find("input", {"name":"_csrf"})["value"]
        login_data = {"LoginForm[username]":config.USERNAME, "LoginForm[password]":config.PASSWORD, "_csrf":token}

        res_page = self.sess.post(login_link, login_data)
        res_content = bs(res_page.content, "html.parser")

        return res_content.find('a', text='Log Off')

    def send_email(self, result, cls_info):
        msg = "Subject: BuzzGym Booking RESULT\n\n"
        if result == BOOKED:
            msg = msg.replace('RESULT', 'Successful')
            msg += 'You have successfully booked %s at %s!' %(cls_info['name'], cls_info['datetime'])
        elif result == WAITLISTED:
            msg = msg.replace('RESULT', 'Waitlisted')
            msg += 'You have been added to the wailist for %s at %s!' %(cls_info['name'], cls_info['datetime'])
        elif result == BAD_FAIL:
            msg = msg.replace('RESULT', 'Bad Fail')
            msg += 'Places are available for %s at %s, but your booking has somehow failed!' %(cls_info['name'], cls_info['datetime'])
        else:
            return

        ## send email
        # Create a secure SSL context
        context = ssl.create_default_context()

        # Try to log in to server and send email
        try:
            server = smtplib.SMTP(config.SMTP_SERVER, config.SMTP_PORT)
            server.starttls(context=context) # Secure the connection
            server.login(config.EMAIL, config.EMAIL_PWD)
            server.sendmail('booking@buzz.gym', config.EMAIL, msg)
        except Exception as e:
            # Print any error messages to stdout
            print(e)
        finally:
            server.quit()

    def book_class(self, constraints=None):
        if constraints is None:
            constraints = self.constraints

        timetable_link = "https://oxfordmembers.buzzgym.co.uk/timetable"
        timetable_page = self.sess.get(timetable_link)
        timetable_content = bs(timetable_page.content, "html.parser")

        for cstr in constraints:
            classes = timetable_content.find_all('a', text=cstr['class_name'])  # not including booked classes, which have ** in text
            for cls in classes:
                cls_link = cls['href']  # eg: 'https://oxfordmembers.buzzgym.co.uk/timetable/1998?t=2020-02-23+13%3A00'
                date = cls_link[len('https://oxfordmembers.buzzgym.co.uk/timetable/1998?t='):-len('+13%3A00')]

                time_avail = cls.next_element.next_element.next_element.next_element.text
                time = time_avail[:5]
                time_t = datetime.datetime.strptime(time, '%H:%M').time()
                avail = time_avail[13:]
                duration = time_avail[:13]

                datetime_t = datetime.datetime.strptime(' '.join([date, time]), '%Y-%m-%d %H:%M')
                cls_info = {'name': cstr['class_name'], 'datetime': datetime_t.strftime("%a %m/%d/%Y ")+duration.replace(' ','')}

                ## not available
                if 'fully booked' in avail:
                    continue

                ## handling time constraints
                time_req = cstr.get('time', None)
                if time_req:
                    time_checks = []
                    for time_cstr in time_req[1]:
                        op = time_cstr[0]
                        req_t = datetime.datetime.strptime(time_cstr[1:], '%H:%M').time()

                        if op == '>':
                            time_satisfy = time_t > req_t
                        elif op == '<':
                            time_satisfy = time_t < req_t
                        elif op == '=':
                            time_satisfy = time_t == req_t
                        else:
                            raise ValueError('time constraints must be formatted as: [<,>,=]HH:MM')
                        time_checks += [time_satisfy]

                    if time_req[0] == '&':
                        time_checks = all(time_checks)
                    elif time_req[0] == '|':
                        time_checks = any(time_checks)
                    else:
                        time_checks = time_checks[0]
                    if not time_checks:
                        continue

                ## handling date constraints
                date_req = cstr.get('date', None)
                if date_req:
                    date_checks = []
                    for date_cstr in date_req[1]:
                        req_d = datetime.datetime.strptime(date_cstr, '%m/%d')
                        date_satisfy = req_d.month==datetime_t.month and req_d.day==datetime_t.day
                        date_checks += [date_satisfy]

                    if not any(date_checks):
                        continue

                ## handling weekday constraints
                weekday_req = cstr.get('weekday', None)
                if weekday_req:
                    weekdays = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
                    weekday_checks = []
                    for weekday_cstr in weekday_req[1]:
                        weekday_satisfy = weekday_cstr.lower() in weekdays[datetime_t.weekday()].lower()
                        weekday_checks += [weekday_satisfy]

                    if not any(weekday_checks):
                        continue

                ## no earlier than MIN_TIME_AHEAD mins from now
                if datetime_t < datetime.datetime.now() + datetime.timedelta(minutes=MIN_TIME_AHEAD):
                    continue

                ## places available
                if 'laces available' in avail or 'lace available' in avail:
                    booklink = cls['href'].replace('timetable', 'timetable/book').replace('t=', 'date=')
                    result_page = self.sess.get(booklink)
                    result_content = bs(result_page.content, "html.parser")
                    cls_booked = result_content.find('h3', text='Class Booked') is not None
                    if cls_booked:
                        self.send_email(BOOKED, cls_info)
                        continue

                    ## booked 5 classes already
                    alert = result_content.find('div', {"class":"alert alert-danger"})
                    if alert is not None and "only 5 future classes can be booked" in alert.text:
                        continue

                    ## booking failed
                    self.send_email(BAD_FAIL, cls_info)

                ## waitlist available
                elif 'list available' in avail:
                    booklink = cls['href'].replace('timetable', 'timetable/joinwl').replace('t=', 'date=')
                    result_page = self.sess.get(booklink)
                    result_content = bs(result_page.content, "html.parser")
                    cls_waitlisted = result_content.find('h3', text='Waiting list reservation confirmation') is not None
                    if cls_waitlisted:
                        self.send_email(WAITLISTED, cls_info)
                        continue

                    ## waitlisting failed
                    self.send_email(BAD_FAIL, cls_info)

                ## not resolved
                else:
                    continue

    def run(self):
        while True:
            self.reset()
            self.login()
            self.load_constraints()
            self.book_class()

            time.sleep(REFRESH_INTERVAL)

if __name__ == '__main__':
    booker = ClassBooker(constraint_file=CONSTRAINT_FILE)
    booker.run()
